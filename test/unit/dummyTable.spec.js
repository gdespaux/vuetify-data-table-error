// Libraries
import Vue from 'vue';
import Vuetify from 'vuetify';

// Components
import DummyTable from '../../src/views/DummyTable';

// Utilities
import { createLocalVue, mount } from '@vue/test-utils';

const localVue = createLocalVue();

describe('DummyTable', function() {
    let vuetify;

    beforeEach(() => {
        vuetify = new Vuetify();
    });

    it('Renders a dummy table', () => {
        const wrapper = mount(DummyTable, {
            localVue,
            vuetify
        });

        expect(wrapper.html()).toMatchSnapshot();
    });
});
