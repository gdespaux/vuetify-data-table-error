import '@babel/polyfill';
import Vue from 'vue';
import vuetify from './plugins/vuetify';
import 'material-design-icons-iconfont/dist/material-design-icons.css';
import App from './App.vue';
import router from '@/router';
import store from '@/store';

let app;

if (!app) {
    app = new Vue({
        vuetify,
        router,
        store,
        render: h => h(App)
    }).$mount('#app');
}
